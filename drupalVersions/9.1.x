  variables:
      drupalDistroRefspec: '9.1.x'
      phpVersion: '7.3.23-fpm-alpine3.12'
      nginxVersion: '1.18.0'
      composerVersion: '2.0.0'
      drushVersion: '10.3.6'

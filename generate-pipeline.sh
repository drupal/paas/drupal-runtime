#!/bin/bash

# We set if image on master is `latest` or `stable`
export IMAGE_VERSION=$1

cat <<EOF
stages:
 - build-nginx
 - build-sitebuilder-base
EOF

# Generating NGINX CI
for versionName in drupalVersions/*
do
    cat <<EOF
job_nginx_${versionName##*/}:
  stage: build-nginx
  needs: []
  rules:
    - if: '\$CI_COMMIT_BRANCH == "master"'
      variables:                              # Set IMAGE_DESTINATION for master branch
        IMAGE_DESTINATION_EXTENSION: "-$IMAGE_VERSION"
    - if: '\$CI_COMMIT_BRANCH != "master" || \$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:                              # Set IMAGE_DESTINATION for feature branches and MR pipelines
        IMAGE_DESTINATION_EXTENSION: -dev-\$CI_COMMIT_SHORT_SHA
  image:
    # We recommend using the CERN version of the Kaniko image: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
    - wget --no-check-certificate https://github.com/mikefarah/yq/releases/download/v4.2.0/yq_linux_amd64 -O /yq && chmod +x /yq
    - echo "{\"auths\":{\"\$CI_REGISTRY\":{\"username\":\"\$CI_REGISTRY_USER\",\"password\":\"\$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - export drupalDistroRefspec=`/yq e .variables.drupalDistroRefspec $versionName`
    - export nginxVersion=`/yq e .variables.nginxVersion $versionName`
    - /kaniko/executor --context \$CI_PROJECT_DIR/nginx --dockerfile \$CI_PROJECT_DIR/nginx/Dockerfile
            --destination \$CI_REGISTRY_IMAGE/nginx:${versionName##*/}\$IMAGE_DESTINATION_EXTENSION
            --build-arg NGINX_VERSION=\$nginxVersion;
    - echo "Pushed to $CI_REGISTRY_IMAGE/nginx:${versionName##*/}\$IMAGE_DESTINATION_EXTENSION"

EOF
    cat $versionName
done


# Generated Site-Builder CI
for versionName in drupalVersions/*
do
    cat <<EOF
job_site_${versionName##*/}:
  stage: build-sitebuilder-base
  needs: []
  rules:
    - if: '\$CI_COMMIT_BRANCH == "master"'
      variables:                              # Set IMAGE_DESTINATION for master branch
        IMAGE_DESTINATION_EXTENSION: "-$IMAGE_VERSION"
    - if: '\$CI_COMMIT_BRANCH != "master" || \$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:                              # Set IMAGE_DESTINATION for feature branches and MR pipelines
        IMAGE_DESTINATION_EXTENSION: -dev-\$CI_COMMIT_SHORT_SHA
  image:
    # We recommend using the CERN version of the Kaniko image: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  script:
    - wget --no-check-certificate https://github.com/mikefarah/yq/releases/download/v4.2.0/yq_linux_amd64 -O /yq && chmod +x /yq
    - echo "{\"auths\":{\"\$CI_REGISTRY\":{\"username\":\"\$CI_REGISTRY_USER\",\"password\":\"\$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - export drupalDistroRefspec=`/yq e .variables.drupalDistroRefspec $versionName`
    - export phpVersion=`/yq e .variables.phpVersion $versionName`
    - export composerVersion=`/yq e .variables.composerVersion $versionName`
    - export drushVersion=`/yq e .variables.drushVersion $versionName`
    - wget https://gitlab.cern.ch/drupal/paas/cern-drupal-distribution/-/archive/\${drupalDistroRefspec}/cern-drupal-distribution-\${drupalDistroRefspec}.tar;
    - tar xf cern-drupal-distribution-\${drupalDistroRefspec}.tar;
    - mv cern-drupal-distribution-\${drupalDistroRefspec} cern-drupal-distribution;
    - /kaniko/executor --context \$CI_PROJECT_DIR --dockerfile \$CI_PROJECT_DIR/Dockerfile
        --destination \$CI_REGISTRY_IMAGE/site-builder-base:${versionName##*/}\$IMAGE_DESTINATION_EXTENSION
        --build-arg PHP_VERSION=\$phpVersion --build-arg COMPOSER_VERSION=\$composerVersion --build-arg DRUSH_VERSION=\$drushVersion;
    - echo "Pushed to $CI_REGISTRY_IMAGE/site-builder-base:${versionName##*/}\$IMAGE_DESTINATION_EXTENSION"

EOF
    cat $versionName
done
